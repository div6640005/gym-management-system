package repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

    public Connection connectionDb() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");

        String url = "jdbc:postgresql://localhost:5432/gym-management-system";
        String username = "postgres";
        String password = "3021569587";

        return DriverManager.getConnection(url, username, password);
    }
}
