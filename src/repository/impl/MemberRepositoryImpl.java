package repository.impl;

import model.Member;
import queries.MemberQueries;
import repository.DbConnection;
import repository.MemberRepository;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class MemberRepositoryImpl extends DbConnection implements MemberRepository {
    @Override
    public boolean addMember(Member member) {
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(MemberQueries.ADD_MEMBER);
            statement.setString(1, member.getName());
            statement.setString(2, member.getSurname());
            statement.setString(3, member.getFin());
            statement.setString(4, member.getSerialNumber());
            statement.setDate(5, Date.valueOf(member.getRegisterDate()));
            statement.setDate(6, Date.valueOf(member.getUpdateDate()));
            statement.setDate(7, Date.valueOf(member.getExpirationDate()));
            statement.setInt(8, member.getAmount());
            statement.setInt(9, member.getDepartures());
            int count = statement.executeUpdate();

            return count > 0;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public void showMember() {
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(MemberQueries.SHOW_MEMBER);
            ResultSet resultSet = statement.executeQuery();
            List<Member> members = new LinkedList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String fin = resultSet.getString("fin");
                String serialNumber = resultSet.getString("serialNumber");
                LocalDate registerDate = resultSet.getDate("registerDate").toLocalDate();
                LocalDate updateDate = resultSet.getDate("updateDate").toLocalDate();
                LocalDate expirationDate = resultSet.getDate("expirationDate").toLocalDate();
                int amount = resultSet.getInt("amount");
                int departures = resultSet.getInt("departures");
                members.add(new Member(id, name, surname, fin, serialNumber, registerDate, updateDate, expirationDate, amount, departures));
            }
            for (int i = 0; i < members.size(); i++) {
                System.out.println(members.get(i));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean updateMember(Member member) {


        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(MemberQueries.UPDATE_MEMBER);
            statement.setString(1, member.getName());
            statement.setString(2, member.getSurname());
            statement.setString(3, member.getFin());
            statement.setString(4, member.getSerialNumber());
            statement.setDate(5, Date.valueOf(member.getRegisterDate()));
            statement.setDate(6, Date.valueOf(member.getUpdateDate()));
            statement.setDate(7, Date.valueOf(member.getExpirationDate()));
            statement.setInt(8, member.getAmount());
            statement.setInt(9, member.getDepartures());
            statement.setInt(10, member.getId());
            int count = statement.executeUpdate();

            return count > 0;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }


        return false;
    }

    @Override
    public Member FindById(int oldID) {
        Member oldMember = new Member();
        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(MemberQueries.FIND_BY_ID);
            statement.setInt(1, oldID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String fin = resultSet.getString("fin");
                String serialNumber = resultSet.getString("serialNumber");
                LocalDate registerDate = resultSet.getDate("registerDate").toLocalDate();
                LocalDate updateDate = resultSet.getDate("updateDate").toLocalDate();
                LocalDate expirationDate = resultSet.getDate("expirationDate").toLocalDate();
                int amount = resultSet.getInt("amount");
                int departures = resultSet.getInt("departures");
                oldMember = new Member(id, name, surname, fin, serialNumber, registerDate, updateDate, expirationDate, amount, departures);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return oldMember;
    }

    @Override
    public void EnterTheGym(String fin) {

        try (Connection connection = connectionDb()) {
            PreparedStatement statement = connection.prepareStatement(MemberQueries.ENTER_THE_GYM);
            statement.setString(1, fin);
            PreparedStatement statement1 = connection.prepareStatement(MemberQueries.DEPARTURES_CHANGER);
            statement1.setString(1, fin);
            statement1.executeUpdate();

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                int departures = resultSet.getInt("departures");
                System.out.println(name + " " + surname + " \" " + departures + " \" departures");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
