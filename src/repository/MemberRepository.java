package repository;

import model.Member;

public interface MemberRepository {
    boolean addMember(Member member);
    void showMember();
    boolean updateMember(Member member);
    Member FindById(int oldID);
    void EnterTheGym(String fin);
}
