package util;

import model.Member;

import java.time.LocalDate;

public class MemberUtil {
    public static Member MemberCreator (){
        return Member.builder().
                name(InputUtil.inputRequiredString("Enter the name: ")).
                surname(InputUtil.inputRequiredString("Enter the surname: ")).
                fin(InputUtil.inputRequiredString("Enter the fin: ")).
                serialNumber(InputUtil.inputRequiredString("Enter the serial number: ")).
                registerDate(LocalDate.now()).
                updateDate(LocalDate.now()).
                expirationDate(LocalDate.parse(InputUtil.inputRequiredString("Enter the LocalDate: "))).
                amount(InputUtil.inputRequiredInteger("Enter the amount: ")).
                departures(InputUtil.inputRequiredInteger("Enter the departures: ")).
                build();
    }
    public static Member MemberUpdater(Member oldMember) {
        return Member.builder().
                id(oldMember.getId()).
                name(oldMember.getName()).
                surname(oldMember.getSurname()).
                fin(oldMember.getFin()).
                serialNumber(oldMember.getSerialNumber()).
                registerDate(oldMember.getRegisterDate()).
                updateDate(LocalDate.now()).
                expirationDate(LocalDate.parse(InputUtil.inputRequiredString("Enter the LocalDate: "))).
                amount(oldMember.getAmount()).
                departures(InputUtil.inputRequiredInteger("Enter the departures: ")).
                build();
    }
}
