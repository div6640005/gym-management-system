package service;

import repository.impl.MemberRepositoryImpl;
import util.InputUtil;

public class ManagementService {
    public static void EntryApp() {
        MemberRepositoryImpl memberRepository = new MemberRepositoryImpl();
        while (true) {
            System.out.println(
                    """
                            
                            ----------| Gym Management System |----------
                            0. exit
                            1. Register gym member
                            2. Show all members
                            3. Enter the gym
                            4. Update departures
                            
                            """);
            int option = InputUtil.inputRequiredInteger("Enter the option: ");

            switch (option) {
                case 0 -> System.exit(-1);
                case 1 -> {
                    if (memberRepository.addMember(util.MemberUtil.MemberCreator()))
                        System.out.println("Register Succesfully");
                }
                case 2 -> memberRepository.showMember();
                case 3 -> memberRepository.EnterTheGym(InputUtil.inputRequiredString("Enter the fin: "));
                case 4 -> memberRepository.updateMember(util.MemberUtil.MemberUpdater(memberRepository.FindById(InputUtil.inputRequiredInteger("Enter the ID: "))));
                default -> System.out.println("Invalid option !!!");
            }
        }
    }
}
