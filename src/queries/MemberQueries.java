package queries;

public class MemberQueries {
    public static final String ADD_MEMBER = "insert into member (name, surname, fin, serialNumber, registerDate, updateDate, expirationDate, amount, departures) VALUES (?,?,?,?,?,?,?,?,?)";
    public static final String SHOW_MEMBER = "SELECT * from  member";
    public static final String ENTER_THE_GYM  = "SELECT name,surname,departures from member where fin = ?;";
    public static final String UPDATE_MEMBER = "UPDATE member set name = ?, surname = ?, fin=?,serialNumber = ?, registerDate = ?, updateDate = ?, expirationDate = ?, amount = ?, departures = ? where id = ?;";
    public static final String FIND_BY_ID = "SELECT * from  member where id = ?";
    public static final String DEPARTURES_CHANGER = "UPDATE member set departures = departures - 1 where fin = ?";

}
