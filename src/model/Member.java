package model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Member {
    int id;
    String name;
    String surname;
    String fin;
    String serialNumber;
    LocalDate registerDate;
    LocalDate updateDate;
    LocalDate expirationDate;
    int amount;
    int departures;


}
